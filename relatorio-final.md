# MAC0218 - Projeto "Doaki - Entrega Final
* O Doaki é uma plataforma onde os usuários se cadastram para poder ofertar doações de posses que não
lhes têm mais serventia de forma que outra pessoa possa fazer melhor proveito desses itens. O contato
se dá através de email ou telefone das partes interessadas, que devem combinar entre si para a entrega
do objeto doado.

### Integrantes
* Caio Túlio de Deus Andrade - 9797232
* Édio Cerati Neto - 9762678
* Leandro Rodrigues - 10723944
* Vinícius Pereira - 10737161

### Instalação e Utilização
* O projeto usa a versão `2.5.1` de Ruby, que pode ser instalada com o gerenciador de versões de Ruby,
rvm
* A seguir, instalar o `bundler` com o comando `gem install bundler`, responsável por gerir e instalar as versões de cada gema de Ruby necessária ao projeto Doaki
* A versão de Rails utilizada é a `5.2.3`, que deve ser instalada com `gem install rails -v 5.2.3`
* Dentro do repositório do projeto clonado, executar `bundler install --without production` para garantir que as dependências de gemas sejam satisfeitas
* É aconselhável executar `rails db:drop` e `rails db:migrate` para garantir que o banco de dados esteja operando corretamente
* O arquivo `seeds.rb` foi configurado com a criação de um usuário administrador do sistema, portanto é necessário executar `rails db:seed` antes de executar o servidor
* As credenciais do admin são:
    * **email/usuário**: `admin@doaki.com`
    * **senha**: `password`
* Executar `rails s` irá subir o servidor, escutando em `localhost:3000`
* Vários testes foram definidos, que podem ser executados com o comando `rails test`
    * São feitas 39 `runs` e 78 `assertions`, todas funcionando, não devendo retornar erro algum

### Repositório no Gitlab
* [https://gitlab.com/tecprog/doaki](https://gitlab.com/tecprog/doaki)

### Deploy no Heroku
* [http://doaki.herokuapp.com](http://doaki.herokuapp.com)
* Caso haja algum problema de visualização ou instalação, Doaki está facilmente acessível no Heroku, com
sua versão em Production sendo a mais recente presente no repositório do Gitlab
* A versão em deploy no Heroku é apenas para facilitar o acesso e cumprir com os fins didáticos, pois possui o problema do disco ser
efêmero (pois se trata da versão gratuita, sem usar um serviço de armazenamento externo). Ou seja, após
aproximadamente 24 horas, as imagens das doações anunciadas podem sumir, por conta da política de
economia de dados e dynos do Heroku, conforme o seguinte artigo:
https://devcenter.heroku.com/articles/active-storage-on-heroku

### Documentação da Arquitetura
* Rotas
    * `/about` (GET): informações sobre o Doaki
    * `/` (GET): root, homepage do Doaki
    * `/donations` (GET): `donations#index`, referente a mostrar todas as doações presentes no banco de dados
    * `/donations` (POST): `donations#create`, referente à criação de uma nova doação ao submeter o formulário
    * `/donations/new` (GET): rota contendo o formulário para criação de uma nova doação
    * `/donations/:id/edit` (GET): rota contendo o formulário para edição de uma doação já existente
    * `/donations/:id` (GET): mostra o conteúdo referente a uma doação
    * `/donations/:id` (PATCH): rota referente ao submit de edição de uma doação
    * `/donations/:id` (PUT): rota referente ao submit de edição de uma doação
    * `/donations/:id` (DELETE): rota referente à deleção de uma doação do banco de dados
    * `/users` (GET): `users#index`, referente a mostrar todos os usuários presentes no banco de dados
    * `/users` (POST): `users#create`, referente à criação de um novo usuário ao submeter o formulário
    * `/signup` (GET): rota contendo o formulário para cadastramento de um novo usuário
    * `/users/:id/edit` (GET): rota contendo o formulário para edição de um usuário já cadastrado
    * `/users/:id` (GET): mostra o conteúdo referente a um usuário (dados e doações associadas)
    * `/users/:id` (PATCH): rota referente ao submit de edição dos dados de um usuário
    * `/users/:id` (PUT): rota referente ao submit de edição dos dados de um usuário
    * `/users/:id` (DELETE): rota referente à deleção da conta de um usuário
    * `/login` (GET): rota referente ao form para login de um usuário
    * `/login` (POST): rota referente ao submit do login de um usuário
    * `/logout` (DELETE): rota referente ao encerramento da sessão atual de um usuário
    * `/comments` (GET): `comments#index`, referente a mostrar todos os comentários presentes no banco de dados
    * `/comments` (POST): `comments#create`, referente à submissão de criação de um novo comentário
    * `/comments/new` (GET): rota contendo o campo para criação de um novo comentário
    * `/comments/:id/edit` (GET): rota contendo o formulário para edição de uma doação já existente
    * `/comments/:id` (GET): mostra o conteúdo de um comentário específico
    * `/comments/:id` (PATCH): rota referente ao submit de edição de um comentário
    * `/comments/:id` (PUT): rota referente ao submit de edição de um comentário
    * `/comments/:id` (DELETE): rota referente à deleção de um comentário
* Controllers
    * `donations_controller.rb`: responsável por implementar as operações básicas de CRUD referentes ao banco de dados contendo as doações
    * `pages_controller.rb`: define as páginas padrões a serem exibidas, como `home` e `about`
    * `application_controller.rb`: define métodos de auxílio, como checar o usuário atualmente logado e outro para checar se há alguém logado
    * `comments_controller.rb`: implementa as operações básicas de CRUD referentes à tabela de comentários, associados às doações
    * `sessions_controller.rb`: gerencia o `login` e `logout` dos usuários
    * `users_controller.rb`: implementa as operações básicas de CRUD referentes à tabela de usuários
* Views
    * Contém partials como `_form.html.erb`, que renderiza o formulário a ser utilizado para editar e criar doações e `_nav.html.erb` referente à navbar presente em todas as telas
    * Contém o código `erb` referentes à renderização de todas as outras telas, como `edit`, `index`,
`new` e `show` para cada um dos tipos de controladores
Há também código `jbuilder` para permitir que requisições do tipo `/donations/:id.json`
retornem o objeto referente à doação ao invés de renderizar o código HTML, funcionando como
API
* Models
    * `donation.rb`
        * Define o modelo esperado para uma doação no banco de dados, bem como a obrigatoriedade da presença de título, descrição, detalhes, telefone, email e CEP
        * Associa doações como pertencentes a usuários
        * Usa regex para validar o telefone e o CEP, e a gema `mail` incluída através do módulo `EmailValidatable` para validar o email inserido
        * Também define a presença de imagens através do uso do `ActiveStorage` de Rails
    * `user.rb`
        * Define o modelo esperado para um usuário do sistema, contendo email exclusivo, nome e senha de 6 a 40 caracteres, bem como definindo se ele é admin ou não do sistema
        * Associa como dono de doações e comentários
    * `comments.rb`
        * Associa como pertencente a usuários e doações

### O que foi feito nessa fase
* Finalização do design, padronização e estilização das telas
* Barra de pesquisa para doações
* Autenticação de usuários, permitindo edição e remoção do conteúdo criado por ele
* Funcionalidade de administrador do sistema, para garantir moderação
* Exibição de doações na Home
* Testes unitários de todas funcionalidades e de integração das principais funcionalidades da aplicação, bem como testes das validações dos modelos

### Principais dificuldades
* Ao aumentar a complexidade do banco de dados, com novos modelos e associações, vários erros surgiram, especialmente nos novos deploys para o Heroku. Foi então percebido que algumas migrações estavam equivocadas e causando erros na criação de algumas tabela, que por algum motivo funcionavam em desenvolvimento mas davam erro em produção. Após cercar e refatorar, o banco de dados se tornou mais confiável e consequentemente o deploy

### Participação dos membros do grupo
* O repositório do git anexado por si só já dá um bom parecer, bem como as issues, já que algumas estão assinaladas para integrantes específicos
    * Caio: validações, página de show para as donations, desenho e planejamento das telas do aplicativo, estilização, código javascript e jquery
    * Leandro: setup inicial, partials, estilização, active_storage, controllers para CRUD, desenho e planejamento das telas, autenticação
    * Vinícius: estilização, forms, navbar, desenho de telas, associações do banco de dados
    * Édio: refatoração das rotas, preparação e deploy no Heroku, validação de email, relatório, testes automatizados, bugfixes