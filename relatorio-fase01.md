# MAC0218 - Projeto "Doaki"
* O Doaki é uma plataforma onde os usuários se cadastram para poder ofertar doações de posses que não lhes têm mais serventia de forma que outra pessoa possa fazer melhor proveito desses itens. O contato se dá através de email ou telefone das partes interessadas, que devem combinar entre si para a entrega do objeto doado.

### Integrantes
* Caio Túlio de Deus Andrade - 9797232
* Édio Cerati Neto - 9762678
* Leandro Rodrigues - 10723944
* Vinícius Pereira - 10737161

### Instalação e Utilização
* O projeto usa a versão `2.5.1` de Ruby, que pode ser instalada com o gerenciador de versões de Ruby, `rvm`
* A seguir, instalar o `bundler` com o comando `gem install bundler`, responsável por gerir e instalar as versões de cada gema de Ruby necessária ao projeto Doaki
* A versão de Rails utilizada é a `5.2.3`, que deve ser instalada com `gem install rails -v 5.2.3`
* Dentro do repositório do projeto clonado, executar `bundler install --without production` para garantir que as dependências de gemas sejam satisfeitas
* Executar `rails s` irá subir o servidor, escutando em `localhost:3000`

### Repositório no Gitlab
* https://gitlab.com/tecprog/doaki

### Deploy no Heroku
* http://doaki.herokuapp.com/ 
* Caso haja algum problema de visualização ou instalação, Doaki está facilmente acessível no Heroku, com sua versão em Production sendo a mais recente presente no repositório do Gitlab
* A versão em deploy no Heroku é apenas para facilitar o acesso, possuindo o problema do disco ser efêmero (pois se trata da versão gratuita, sem usar um serviço de armazenamento externo). Ou seja, após aproximadamente 24 horas, as imagens das doações anunciadas podem sumir, por conta da política de economia de dados e dynos do Heroku, conforme o seguinte artigo:
    * https://devcenter.heroku.com/articles/active-storage-on-heroku

### Documentação da Arquitetura
* **Rotas**
    * `/about` (GET): informações sobre o Doaki
    * `/` (GET): root, homepage do Doaki
    * `/donations` (GET): `donations#index`, referente a mostrar todas as doações presentes no banco de dados
    * `/donations` (POST): `donations#create`, referente à criação de uma nova doação ao submeter o formulário
    * `/donations/new` (GET): rota contendo o formulário para criação de uma nova doação
    * `/donations/:id/edit` (GET): rota contendo o formulário para edição de uma doação já existente
    * `/donations/:id` (GET): mostra o conteúdo referente a uma doação
    * `/donations/:id` (PATCH): rota referente ao submit de edição de uma doação
    * `/donations/:id` (PUT): rota referente ao submit de edição de uma doação
    * `/donations/:id` (DELETE): rota referente à deleção de uma doação do banco de dados
* **Controllers**
    * `donations_controller.rb`: responsável por implementar as operações básicas de CRUD referentes ao banco de dados contendo as doações
    * `pages_controller.rb`: define as páginas padrões a serem exibidas, como `home` e `about`
* **Views**
    * Contém *partials* como `_form.html.erb`, que renderiza o formulário a ser utilizado para editar e criar doações e `_nav.html.erb` referente à navbar presente em todas as telas
    * Contém o código `erb` referentes à renderização de todas as outras telas, como `edit`, `index`, `new` e `show`
    * Há também código `jbuilder` para permitir que requisições do tipo `/donations/:id.json` retornem o objeto referente à doação ao invés de renderizar o código HTML, funcionando como API
* **Models**
    * `donation.rb`
        * Define o modelo esperado para uma doação no banco de dados, bem como a obrigatoriedade da presença de título, descrição, detalhes, telefone, email e CEP
        * Usa regex para validar o telefone e o CEP, e a gema `mail` incluída através do módulo `EmailValidatable` para validar o email inserido
        * Também define a presença de imagens através do uso do `ActiveStorage` de Rails

### O que foi feito nessa fase
* Definição do design básico da aplicação
* Criação das rotas a serem utilizadas, bem como implementação das operações CRUD nos respectivos controladores
* Definição do modelo de doação e validações para inserção no banco de dados
* Estilização básica para diretriz do design
* Criação de *partials* úteis que são reutilizadas em várias telas (forms e navbar)
* Instalação e configuração do `ActiveStorage` para permitir o envio de imagens ilustrativas das doações
* Configuração dos *views* necessários para criar, mostrar, listar todos, editar e deletar as doações
* Adição de uma nova versão à navbar, já reservando espaço para a barra de pesquisa por doações. A pesquisa será implementada para a próxima fase.
* Preparação da aplicação para deploy em `production`
* Deploy e debugging no Heroku
* Refatoração de algumas rotas para reduzir a redundância do código,centralizando as rotas não relacionadas à doações em `pages_controller.rb`

### Próximos passos
* Finalizar o design, padronizando a estilização em todas as telas
* Adicionar uma pesquisa funcional dentre todas as doações no banco de dados
* Adicionar autenticação de usuário para permitir remoção da doação pelo criador
* Criar uma exibição de doações na Home, algo como as mais recentes ou mais relevantes para o usuário em questão
* Fazer um `favicon` para o projeto

### Principais dificuldades
* Deploy no Heroku gerou grandes dificuldades, pois algo estava causando incompatibilidade entre as tabelas criadas, visto que Heroku não suporta `sqlite3` e foi necessário várias tentativas de reset e configuração de variáveis de ambiente para enfim o aplicativo funcionar em production

### Participação dos membros do grupo
* O repositório do git anexado por si só já dá um bom parecer, bem como as issues, já que algumas estão assinaladas para integrantes específicos
* **Caio**: validações, página de `show` para as donations, desenho e planejamento das telas do aplicativo
* **Leandro**: setup inicial, partials, estilização, active_storage, controllers para CRUD, desenheo e planejamento das telas
* **Vinícius**: estilização, forms, navbar, desenho de telas
* **Édio**: refatoração das rotas, preparação e deploy no Heroku, validação de email, relatório